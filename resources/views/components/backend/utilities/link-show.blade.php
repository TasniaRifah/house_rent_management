@props(['linkName'=>'Show','icon'])
<a class="btn btn-sm btn-primary" {{$attributes}}>
 @if($icon)
    <i class='{{$icon}}'></i>
    @else
    {{$linkName}}
 @endif   


</a>
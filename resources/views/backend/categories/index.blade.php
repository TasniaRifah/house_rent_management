<x-backend.layout.master>
    <x-slot:title>
        Categories
        </x-slot:>
        <x-slot:pagetitle>
            Categories
            </x-slot:>
            <div class="card mb-4">

                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Category List
                    <a href="{{route('categories.create')}}" class="btn btn-sm btn-primary">Add New</a>
                    <a class="btn btn-sm btn-info" href="{{route('categories.trash') }}">Trash</a>
                    <a class="btn btn-sm btn-info" href="{{route('categories.download_pdf') }}">Pdf</a>
                    <a class="btn btn-sm btn-info" href="{{route('categories.download_excel') }}">Excel</a>
                </div>
                <div class="card-body">
                  <x-backend.alerts.message type="success" :message="session('message')" class="text-dark"/>
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Category Name</th>
                                <th>Description</th>
                                <th>Is_active</th>
                                <th>Image</th>

                                <th>Action</th>

                            </tr>
                        </thead>

                        <tbody>
                            {{-- {{dd($categories)}} --}}

                            @foreach($categories as $category)
                            <tr>
                                <td>{{$loop->iteration}} </td>
                                <td>{{$category->categories_name}}</td>
                                <td>{{$category->description}}</td>
                                <td>{{$category->is_active}}</td>
                              
                                <td>
                                    <x-backend.utilities.link-show href="{{route('categories.show',['category'=>$category->id])}}" icon="fas fa-eye"/>
                                    <x-backend.utilities.link-edit href="{{route('categories.edit',['category'=>$category->id])}}" icon="fas fa-edit"/>
                                   
                                    <form action="{{route('categories.destroy',['category'=>$category->id])}}" method="post" style="display:inline">
                                    @csrf 
                                    @method('delete')  
                                    <x-backend.forms.button  text='Delete' color='danger' icon='fas fa-trash' onclick="return confirm('Are you sure want to delete?')"/>
                                    </form>
                                   
                                </td>

                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @push('css')
            <style>
            /* body{
                background-color: blue;
            } */
            </style>
            @endpush
            @push('js')
            <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
            <script src="{{asset('ui/backend/js/datatables-simple-demo.js')}}"></script>
            @endpush
</x-backend.layout.master>
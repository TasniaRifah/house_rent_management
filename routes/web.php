<?php

use App\Http\Controllers\AuthController;
// use App\Http\Controllers\PublicController;
use App\Http\Controllers\BackendController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//controller&name routing*****************************************
Route::get('Front/index', [PublicController::class, 'index'])->name('Homepage');
Route::get('Front/cart', [PublicController::class, 'cart'])->name('Cart');
Route::get('Front/checkout', [PublicController::class, 'checkout'])->name('Check');
Route::get('Front/order', [PublicController::class, 'order'])->name('order');
Route::get('Front/shop', [PublicController::class, 'shop'])->name('Shop');
Route::get('Front/checkout', [PublicController::class, 'product'])->name('Product');
Route::get('/about', [PublicController::class, 'about'])->name('About');

Route::get('/sign-in', [AuthController::class, 'login'])->name('sign_in');
Route::get('/sign-up', [AuthController::class, 'registration'])->name('sign_up');
//route group*****************************************************
// Route::controller(ProductsController::class)->prefix('products')->group(function(){
//     Route::get('/','index')->name('pro_index');
//     Route::get('/create','create')->name('pro_create');
//     Route::get('/show','show')->name('pro_show');
// });
//middleware******************************************************
//Route::get('/admin',[DashboardController::class,'dashboard' ])->middleware('admin');
// Route::middleware('admin')->group(function(){
//     Route::get('/admin',[DashboardController::class,'dashboard' ]);
//     Route::controller(ProductsController::class)
//     ->name('products.')
//     ->prefix('products')
//     ->group(function(){
//         Route::get('/','index')->name('index');
//         // Route::get('/create','create')->name('create');
//         Route::get('/show','show')->name('show');
//     });
//     // Route::fallback(function () {
//     //     echo "Sorry";
//     // });
// });
//basic routing***************************************************
// Route::get('/', function () {
//     return view('index');
// });
// Route::get('/index', function () {
//     return "hello world";
// });
//view route*****************************
Route::view('/', 'index');
//fallback route*************************
// Route::fallback(function () {
//     echo "Sorry";
// });

//redirect*******************************
//Route::redirect('/here', '/ounnokisu');
//require parameter*********************************************************
Route::get('/products/{id?}', [ProductsController::class, 'show']);
Route::get('/products/create', [ProductsController::class, 'create']);
Route::get('/frontend/index', [FrontendController::class, 'index']);
Route::get('/components/frontend/layout/master', [FrontendController::class, 'master'])->name('frontend.master');
Route::get('/frontend/detail', [FrontendController::class, 'detail']);
Route::get('/components/backend/layout/master', [BackendController::class, 'master'])->name('backend.master');
Route::get('/backend/index', [BackendController::class, 'index']);
Route::get('/backend/product/index', [ProductController::class, 'index']);

Route::prefix('backend')->group(function () {
    Route::get('/', [BackendController::class, 'index'])->name('backend.index');
    Route::get('/product', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
    
    // Route::get('/colors', [ColorController::class, 'index'])->name('colors.index');
    // Route::get('/colors/create', [ColorController::class, 'create'])->name('colors.create');
    // Route::post('/colors', [ColorController::class, 'store'])->name('colors.store');
    // Route::get('/colors/{color}', [ColorController::class, 'show'])->name('colors.show');
    // Route::get('/colors/{color}/edit', [ColorController::class, 'edit'])->name('colors.edit');
    // Route::patch('/colors/{color}/edit', [ColorController::class, 'update'])->name('colors.update');
    // Route::delete('/colors/{color}', [ColorController::class, 'destroy'])->name('colors.destroy');
 
    Route::get('/categories/trash', [CategoryController::class, 'trash'])->name('categories.trash');
    Route::patch('/categories/trash/{id}', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::delete('/categories/trash/{id}', [CategoryController::class, 'delete'])->name('categories.delete');
    Route::get('categories/download-pdf', [CategoryController::class, 'downloadPdf'])->name('categories.download_pdf');
    Route::get('categories/download-excel', [CategoryController::class, 'downloadExcel'])->name('categories.download_excel');
    
    Route::get('/courses/trash', [CourseController::class, 'trash'])->name('courses.trash');
    Route::get('courses/download-pdf', [CourseController::class, 'downloadPdf'])->name('courses.download_pdf');
    Route::get('courses/download-excel', [CourseController::class, 'downloadExcel'])->name('courses.download_excel');
    Route::resource('courses', CourseController::class);
    Route::resource('colors', ColorController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('sizes', SizeController::class);
    Route::resource('tags', TagController::class);
    Route::resource('districts', DistrictController::class); 
    Route::resource('brands', BrandController::class);
});
Route::prefix('admin')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('admin.index');

});

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
        return view('frontend.index');
    }

    public function master(){
        return view('components.frontend.layout.master');
    }
    public function detail(){
        return view('frontend.detail');
    }
}
